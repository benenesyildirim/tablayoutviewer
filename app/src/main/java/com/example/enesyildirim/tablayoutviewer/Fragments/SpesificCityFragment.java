package com.example.enesyildirim.tablayoutviewer.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.enesyildirim.tablayoutviewer.ApiTools.API;
import com.example.enesyildirim.tablayoutviewer.ApiTools.RequestInterface;
import com.example.enesyildirim.tablayoutviewer.Datas.CityTempatures;
import com.example.enesyildirim.tablayoutviewer.Datas.MainData;
import com.example.enesyildirim.tablayoutviewer.R;
import com.squareup.picasso.Picasso;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpesificCityFragment extends Fragment {

    private TextView dayText, nameText, descText, cityNameText, min, max, morn, night, eve, humidity;
    private ImageView imageOfWeather;
    View view;
    MainData mResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.spesific_city, container, false);
        initViews();

        RequestInterface requestInterface = API.getClient().create(RequestInterface.class);
        Call<MainData> call = requestInterface.getWeather("Gaziantep", "1");

        call.enqueue(new Callback<MainData>() {
            @Override
            public void onResponse(Call<MainData> call, Response<MainData> response) {
                if (response.isSuccessful()) {
                    mResponse = response.body();
                    fillWeatherDetail();
                }
            }

            @Override
            public void onFailure(Call<MainData> call, Throwable t) {

            }
        });
        return view;
    }

    private void initViews() {
        dayText = view.findViewById(R.id.dayTV);
        nameText = view.findViewById(R.id.nameTV);
        descText = view.findViewById(R.id.descTV);
        cityNameText = view.findViewById(R.id.cityNameTV);
        min = view.findViewById(R.id.minTV);
        max = view.findViewById(R.id.maxTV);
        morn = view.findViewById(R.id.mornTV);
        night = view.findViewById(R.id.nightTV);
        eve = view.findViewById(R.id.eveTV);
        humidity = view.findViewById(R.id.humidityTV);
        imageOfWeather = view.findViewById(R.id.weatherImage);
    }

    @SuppressLint("SetTextI18n") // Bana ait değil :D
    private void fillWeatherDetail() {
        CityTempatures temp = mResponse.getList().get(0).getTemp();

        dayText.setText(String.valueOf(temp.getDay()));
        nameText.setText(mResponse.getList().get(0).getWeather().get(0).getMain());
        descText.setText(mResponse.getList().get(0).getWeather().get(0).getDescription());
        cityNameText.setText(mResponse.getCity().getName());
        min.setText(getContext().getResources().getString(R.string.minimum, temp.getMin()));
        max.setText(getContext().getResources().getString(R.string.maximum, temp.getMax()));
        morn.setText(getContext().getResources().getString(R.string.morning, temp.getMorn()));
        night.setText(getContext().getResources().getString(R.string.night, temp.getNight()));
        eve.setText(getContext().getResources().getString(R.string.evening, temp.getEve()));
        humidity.setText(getContext().getResources().getString(R.string.humidity, mResponse.getList().get(0).getHumidity()));
        Picasso.get().load("http://openweathermap.org/img/w/" + mResponse.getList().get(0).getWeather().get(0).getIcon() + ".png").fit().placeholder(R.drawable.weather_icon).into(imageOfWeather);
    }
}
