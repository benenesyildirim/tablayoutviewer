package com.example.enesyildirim.tablayoutviewer.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.enesyildirim.tablayoutviewer.ApiTools.API;
import com.example.enesyildirim.tablayoutviewer.ApiTools.RequestInterface;
import com.example.enesyildirim.tablayoutviewer.Datas.CityTempatures;
import com.example.enesyildirim.tablayoutviewer.Datas.MainData;
import com.example.enesyildirim.tablayoutviewer.R;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment implements View.OnClickListener {

    private MainData mResponse;
    private View view;
    private TextView dayText, nameText, descText, cityNameText, min, max, morn, night, eve, humidity;
    private EditText searchET;
    private ImageView imageOfWeather;
    private LinearLayout cityDetailLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.search_fragment, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        dayText = view.findViewById(R.id.dayTV);
        nameText = view.findViewById(R.id.nameTV);
        descText = view.findViewById(R.id.descTV);
        cityNameText = view.findViewById(R.id.cityNameTV);
        min = view.findViewById(R.id.minTV);
        max = view.findViewById(R.id.maxTV);
        morn = view.findViewById(R.id.mornTV);
        night = view.findViewById(R.id.nightTV);
        eve = view.findViewById(R.id.eveTV);
        humidity = view.findViewById(R.id.humidityTV);
        imageOfWeather = view.findViewById(R.id.weatherImage);
        cityDetailLayout = view.findViewById(R.id.cityDetail);
        searchET = view.findViewById(R.id.citySearchET);
        Button searchBtn = view.findViewById(R.id.searchBTN);
        searchBtn.setOnClickListener(this);
    }

    private void fillWeatherDetail() {
        CityTempatures temp = mResponse.getList().get(0).getTemp();

        dayText.setText(String.valueOf(temp.getDay()));
        nameText.setText(mResponse.getList().get(0).getWeather().get(0).getMain());
        descText.setText(mResponse.getList().get(0).getWeather().get(0).getDescription());
        cityNameText.setText(mResponse.getCity().getName());
        min.setText(getContext().getResources().getString(R.string.minimum, temp.getMin()));
        max.setText(getContext().getResources().getString(R.string.maximum, temp.getMax()));
        morn.setText(getContext().getResources().getString(R.string.morning, temp.getMorn()));
        night.setText(getContext().getResources().getString(R.string.night, temp.getNight()));
        eve.setText(getContext().getResources().getString(R.string.evening, temp.getEve()));
        humidity.setText(getContext().getResources().getString(R.string.humidity, mResponse.getList().get(0).getHumidity()));
        Picasso.get().load("http://openweathermap.org/img/w/" + mResponse.getList().get(0).getWeather().get(0).getIcon() + ".png").fit().placeholder(R.drawable.weather_icon).into(imageOfWeather);
    }

    @Override
    public void onClick(View view) {
        String cityName = searchET.getText().toString().trim();

        RequestInterface requestInterface = API.getClient().create(RequestInterface.class);
        Call<MainData> call = requestInterface.getWeather(cityName, "1");

        call.enqueue(new Callback<MainData>() {
            @Override
            public void onResponse(Call<MainData> call, Response<MainData> response) {
                if (response.isSuccessful()) {
                    cityDetailLayout.setVisibility(View.VISIBLE);
                    mResponse = response.body();
                    fillWeatherDetail();
                    closeKeyboard();
                } else {
                    Toast.makeText(getContext(), "Please recontrol your city name!!", Toast.LENGTH_SHORT).show();
                    closeKeyboard();
                }
            }

            @Override
            public void onFailure(Call<MainData> call, Throwable t) {
                //TODO Burayı ne için yazıyoruz!!
            }
        });
    }

    private void closeKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
