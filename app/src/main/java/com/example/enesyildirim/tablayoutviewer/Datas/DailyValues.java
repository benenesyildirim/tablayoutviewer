package com.example.enesyildirim.tablayoutviewer.Datas;

import java.util.List;

public class DailyValues {
private CityTempatures temp;
private List<CityWeatherDesc> weather;
private int humidity;

    public CityTempatures getTemp() {
        return temp;
    }

    public void setTemp(CityTempatures temp) {
        this.temp = temp;
    }

    public List<CityWeatherDesc> getWeather() {
        return weather;
    }

    public void setWeather(List<CityWeatherDesc> weather) {
        this.weather = weather;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
}
