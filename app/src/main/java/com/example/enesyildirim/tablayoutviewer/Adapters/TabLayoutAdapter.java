package com.example.enesyildirim.tablayoutviewer.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabLayoutAdapter extends FragmentPagerAdapter {//TODO Bu ne işe yarıyor ya bomboş!!

    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    public TabLayoutAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addLayout(Fragment fragment) {
        fragmentList.add(fragment);
    }
}
