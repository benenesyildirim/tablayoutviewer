package com.example.enesyildirim.tablayoutviewer.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.enesyildirim.tablayoutviewer.Datas.MainData;
import com.example.enesyildirim.tablayoutviewer.R;
import com.squareup.picasso.Picasso;

public class RecycleViewAdapter extends RecyclerView.Adapter {

    private MainData mainData;
    private RowListener rowListener;

    public RecycleViewAdapter(MainData mainData,RowListener rowListener) {
        this.mainData = mainData;
        this.rowListener = rowListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_design, null);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rowListener != null) {
                    rowListener.onClickRowListener(mainData);
                }
            }
        });

        ListHolder listHolder = ((ListHolder) viewHolder);
        listHolder.nameTv.setText(mainData.getCity().getName());
        listHolder.descTv.setText(mainData.getList().get(position).getWeather().get(0).getDescription());
        listHolder.dayTv.setText(String.valueOf(mainData.getList().get(position).getTemp().getDay()));
        Picasso.get().load("http://openweathermap.org/img/w/" + mainData.getList().get(position)
                .getWeather().get(0).getIcon() + ".png").fit().placeholder(R.drawable.weather_icon)
                .into(listHolder.weatherImage);
    }

    @Override
    public int getItemCount() {
        return mainData.getList().size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        TextView nameTv,descTv,dayTv;
        ImageView weatherImage;

        ListHolder(@NonNull View v) {
            super(v);

            nameTv = v.findViewById(R.id.mainText);
            descTv = v.findViewById(R.id.descText);
            dayTv = v.findViewById(R.id.dayText);
            weatherImage = v.findViewById(R.id.weatherIcon);


        }
    }

    public interface RowListener{
        void onClickRowListener(MainData mainData);
    }
}