package com.example.enesyildirim.tablayoutviewer;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.enesyildirim.tablayoutviewer.Adapters.TabLayoutAdapter;
import com.example.enesyildirim.tablayoutviewer.Fragments.CurrentCityWeatherFragment;
import com.example.enesyildirim.tablayoutviewer.Fragments.SearchFragment;
import com.example.enesyildirim.tablayoutviewer.Fragments.SpesificCityFragment;

public class TabLayoutActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);

        viewPager = findViewById(R.id.viewPager);//TODO Bu dört satır için kısa bir açıklama alabilir miyim?
        setupPageView(viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.grid_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.horizontal_icon);
        tabLayout.getTabAt(2).setIcon(R.drawable.vertical_icon);

    }

    private void setupPageView(ViewPager viewPager) {
        TabLayoutAdapter tabLayoutAdapter = new TabLayoutAdapter(getSupportFragmentManager());
        tabLayoutAdapter.addLayout(new CurrentCityWeatherFragment());
        tabLayoutAdapter.addLayout(new SearchFragment());
        tabLayoutAdapter.addLayout(new SpesificCityFragment());
        viewPager.setAdapter(tabLayoutAdapter);
    }
}
